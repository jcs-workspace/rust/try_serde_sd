use serde::{Deserialize, Serialize};

type RegionBeg = u64;
type RegionEnd = u64;

#[derive(Serialize, Deserialize)]
pub struct Region {
    pub start: Option<u64>,
    pub end: Option<u64>,
}

impl Region {
    pub fn new(_start: Option<u64>, _end: Option<u64>) -> Self {
        Self {
            start: _start,
            end: _end,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct User {
    pub username: String,
    pub path: Option<String>, // the user's location
    pub point: Option<u64>,
    pub region: Option<Region>,
}

impl User {
    pub fn new(_username: String) -> Self {
        Self {
            username: _username,
            path: None,
            point: None,
            region: None,
        }
    }

    pub fn update(&mut self, path: Option<String>, point: Option<u64>, region: Option<Region>) {
        self.path = path;
        self.point = point;
        self.region = region;
    }
}

fn main() {
    let mut user = User::new("Hello".to_string());

    user.update(
        Some("/path/to/project/".to_string()),
        Some(10),
        Some(Region::new(Some(10), Some(30))),
    );

    let mut arr = Vec::new();
    arr.push(user);

    let data = serde_json::to_string(&arr);

    println!("{:?}", data);
}
